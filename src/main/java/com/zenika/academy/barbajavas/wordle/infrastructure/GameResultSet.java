package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.game.GameNotForThisUserException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GameResultSet implements ResultSetExtractor<Game> {

    @Override
    public Game extractData(ResultSet rs) throws SQLException, DataAccessException {

        Game result = null;
        while(rs.next()) {
            final String tid = rs.getString("tid");
            final String random_word = rs.getString("random_word");
            final int maxAttempts = rs.getInt("max_attempts");
            //final int round_order = rs.getInt("round_order");
            final String input_word = rs.getString("input_word");
            final String userTid = rs.getString("user_id");

            if(result == null) {
                result = new Game(tid, random_word, maxAttempts, userTid);
            }

            try {
                if(input_word != null) {
                result.guess(input_word, userTid);}
            } catch (GameNotForThisUserException e) {
               throw new RuntimeException("ça n'arrive pas parce que le user existe");
            }
        }
        return result;
    }
}
