package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class JdbcGameRepository implements GameRepository {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcGameRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(Game game) {
        jdbcTemplate.update("INSERT INTO games(tid, random_word, max_attempts, user_id) VALUES(?, ?, ?, ?) ON CONFLICT DO NOTHING",
                game.getTid(), game.getWord(true), game.getMaxAttempts(), game.getUserTid().orElse(null) );
        //ajouter un insert into dans la save pour les rounds avec une boucle for
        for (int i = 0; i < game.getRounds().size(); i++) {
            jdbcTemplate.update("INSERT INTO rounds(input_word, round_order, game_id) VALUES(?, ?, ?) ON CONFLICT DO NOTHING",
                   String.valueOf(game.getRounds().get(i).letters()),
                    i + 1,
                    game.getTid());
        }
    }


    @Override
    public Optional<Game> findByTid(String tid) {
        //select * from games left join round_results rr on games.tid = rr.game_tid where tid='' order by round_order asc --> retourne deux lignes
        Game g = jdbcTemplate.query("SELECT games.tid, games.random_word, games.max_attempts, games.user_id, rounds.input_word, " +
                "rounds.round_order FROM games LEFT JOIN rounds ON rounds.game_id = ? ORDER BY rounds.round_order asc", new GameResultSet(), tid);
        return Optional.ofNullable(g);
    }

    @Override
    public List<Game> findByUserTid(String userTid) {
        return null;
    }
}
