package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public interface GameRepository {
    public void save(Game game);
    
    public Optional<Game> findByTid(String tid);

    public List<Game> findByUserTid(String userTid);

    /*
    private Boolean userOwnsGame(String userTid, Game g) {
        return g.getUserTid().map(tid -> tid.equals(userTid)).orElse(false);
    }
     */
}

