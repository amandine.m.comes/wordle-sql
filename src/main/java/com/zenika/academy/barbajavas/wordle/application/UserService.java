package com.zenika.academy.barbajavas.wordle.application;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.users.UserNotFoundException;
import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.repository.UserRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserService {

    private final UserRepository userRepository;
    private final GameRepository gameRepository;

    public UserService(UserRepository userRepository, GameRepository gameRepository) {
        this.userRepository = userRepository;
        this.gameRepository = gameRepository;
    }

    public User createUser(String email, String username) {
        User newUser = new User(UUID.randomUUID().toString(), email, username);
        userRepository.save(newUser);
        
        return newUser;
    }

    public Optional<User> findByTid(String userTid) {
        return userRepository.findByTid(userTid);
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void changeUsername(String userTid, String newUsername) throws UserNotFoundException {
        User user = userRepository.findByTid(userTid).orElseThrow(UserNotFoundException::new);
        
        user.setUsername(newUsername);
        
        userRepository.changeUsername(userTid, newUsername);
    }

    public void deleteUser(String userTid) {
        userRepository.delete(userTid);
    }

    public List<Game> findGames(String userTid) {
        return gameRepository.findByUserTid(userTid);
    }
}
