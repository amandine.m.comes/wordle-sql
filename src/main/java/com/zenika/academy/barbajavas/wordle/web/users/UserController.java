package com.zenika.academy.barbajavas.wordle.web.users;

import com.zenika.academy.barbajavas.wordle.application.UserService;
import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.users.UserNotFoundException;
import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    ResponseEntity<User> createUser(@RequestBody CreateUserRequestDto body) {
        User createdUser = userService.createUser(body.email(), body.username());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    @GetMapping("/{userTid}")
    ResponseEntity<User> findUser(@PathVariable String userTid) {
        return userService.findByTid(userTid).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping("/{userTid}/games")
    List<Game> findUserGames(@PathVariable String userTid) {
        return userService.findGames(userTid);
    }

    @GetMapping
    List<User> searchUsers(@RequestParam("email") String email) {
        return userService.findByEmail(email)
                .map(List::of)
                .orElseGet(Collections::emptyList);
    }

    @PatchMapping("/{userTid}")
    ResponseEntity<Void> changeUsername(@PathVariable String userTid, @RequestBody ChangeUsernameRequestDto body) {
        try {
            userService.changeUsername(userTid, body.newUsername());
            return ResponseEntity.ok().build();
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{userTid}")
    @ResponseStatus(HttpStatus.OK)
    void deleteUser(@PathVariable String userTid) {
        userService.deleteUser(userTid);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleIllegalArgument(IllegalArgumentException e) {
        return "Missing information : " + e.getMessage();
    }
}
