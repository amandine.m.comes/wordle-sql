package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class JdbcUserRepository implements UserRepository {
    private JdbcTemplate jdbcTemplate;


    @Autowired
    public JdbcUserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(User u) {
        final String tid = u.getTid();
        final String email = u.getEmail();
        final String username = u.getUsername();
        jdbcTemplate.update("insert into users(tid, email, name) values(?,?,?)", tid, email, username);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        // select * from users where users.email = email
        User u = jdbcTemplate.queryForObject("select * from users where users.email = ?", new UserRowMapper(), email);
        return Optional.ofNullable(u);
    }

    @Override
    public Optional<User> findByTid(String userTid) {
        // select * from users where users.tid = userTid
        User u = jdbcTemplate.queryForObject("select * from users where users.tid = ?", new UserRowMapper(), userTid);
        return Optional.ofNullable(u);
    }

    @Override
    public void delete(String userTid) {
        // delete from users where users.tid = userTid
        jdbcTemplate.update("delete from users where users.tid = ?", userTid);
    }

    @Override
    public void changeUsername(String userTid, String newUsername){
        jdbcTemplate.update("UPDATE users SET name = ? WHERE tid = ?", newUsername, userTid);
    }
}
//insert into users(tid, email, name) values(?,?,?)
//on conflict(email) do update set name = excluded.name"