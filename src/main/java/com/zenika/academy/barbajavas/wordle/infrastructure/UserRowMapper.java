package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User>{
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        final String userTid = rs.getString("tid");
        final String email = rs.getString("email");
        final String username = rs.getString("name");
        return new User(userTid, email, username);
    }
}

