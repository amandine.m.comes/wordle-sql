package com.zenika.academy.barbajavas.wordle.domain.model.users;

import org.springframework.util.StringUtils;

public class User {
    private final String tid;
    private String email;
    private String username;

    public User(String tid, String email, String username) {
        this.tid = tid;
        this.username = username;
        this.email = email;
    }

    public String getTid() {
        return tid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        if(!StringUtils.hasText(username)) {
            throw new IllegalArgumentException("Username can't be null");
        }
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(!StringUtils.hasText(email)) {
            throw new IllegalArgumentException("Email can't be null");
        }
        this.email = email;
    }
}
