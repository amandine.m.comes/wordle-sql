package com.zenika.academy.barbajavas.wordle.domain.model.game;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Game {
    private final String tid;
    private final String word;
    private final int maxAttempts;
    private final List<RoundResult> roundResults;
    private final Optional<String> userTid;

    public Game(String tid, String word, int maxAttempts, String userTid) {
        this.userTid = Optional.ofNullable(userTid);
        this.tid = tid;
        this.word = word;
        this.maxAttempts = maxAttempts;
        this.roundResults = new ArrayList<>(maxAttempts);
    }

    public String getTid() {
        return this.tid;
    }
    
    public Optional<String> getUserTid() {
            return this.userTid;
    }

    public int getAttemptsLeft() {
        return this.maxAttempts - this.roundResults.size();
    }

    public int getMaxAttempts() {
        return this.maxAttempts;
    }


    public GameState getGameState() {
        if (roundResults.isEmpty()) {
            return GameState.IN_PROGRESS;
        } else if (roundResults.get(roundResults.size() - 1).isWin()) {
            return GameState.WIN;
        } else {
            return roundResults.size() < maxAttempts ? GameState.IN_PROGRESS : GameState.LOSS;
        }
    }

    public void guess(String userInput) throws GameNotForThisUserException {
        this.guess(userInput, null);
    }
    
    public void guess(String userInput, String userTid) throws GameNotForThisUserException {
        if(Objects.equals(this.userTid.orElse(null), userTid)) {
            this.roundResults.add(RoundResult.fromGuess(this.word, userInput));
        }
        else {
            throw new GameNotForThisUserException();
        }
        
    }

    public int getWordLength() {
        return word.length();
    }

    public List<RoundResult> getRounds() {
        return Collections.unmodifiableList(this.roundResults);
    }
    
    public String getWord() {
        return this.getGameState() == GameState.IN_PROGRESS ? "?" : this.word;
    }

    public String getWord(boolean force) {
        if (force) {
            return this.word;
        }
        else {
            return this.getWord();
        }
    }
    
    public Set<Character> getInvalidLetters() {
        return this.roundResults.stream()
                .flatMap(rr -> rr.invalidLetters().stream())
                .collect(Collectors.toSet());
    }
}
