package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


public interface UserRepository {
    public void save(User u);
    
    public Optional<User> findByEmail(String email);

    public Optional<User> findByTid(String userTid);

    public void delete(String userTid);

    public void changeUsername(String userTid, String newUsername);
}
