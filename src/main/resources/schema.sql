-- table creation
CREATE TABLE IF NOT EXISTS users
(
    tid         char(36) primary key,
    email       text not null unique,
    name        text not null
);

CREATE TABLE IF NOT EXISTS games
(
    tid          char(36) primary key,
    random_word  text not null,
    max_attempts int not null,
    user_id      char(36) REFERENCES users(tid)
);

CREATE TABLE IF NOT EXISTS rounds
(
    input_word  text,
    round_order int CHECK (round_order > 0),
    game_id     char(36) REFERENCES games(tid),
    PRIMARY KEY(input_word, round_order)
);
