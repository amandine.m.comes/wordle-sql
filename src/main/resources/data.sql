-- insert data

-- insérer un user
INSERT INTO users(tid, email, name)
VALUES('de10ec32-862c-4bb6-8f12-0eaf68ab96e4', 'bob@mail.com', 'bob') on conflict do nothing;

-- insérer un jeu
INSERT INTO games(tid, random_word, max_attempts, user_id)
VALUES('92528b7c-cd9b-471b-be7a-5dcfa5c231d3', 'ahuri', 6, 'de10ec32-862c-4bb6-8f12-0eaf68ab96e4') on conflict do nothing;

-- insérer un round
INSERT INTO rounds(input_word, round_order, game_id)
VALUES('table', 1, '92528b7c-cd9b-471b-be7a-5dcfa5c231d3') on conflict do nothing;